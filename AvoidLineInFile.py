# reused to produce a rule to warn about lineinfile 04/2017
# Chemistry Computer Officers support@ch.cam.ac.uk
#
# Copyright (c) 2016 Will Thames <will@thames.id.au>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from ansiblelint.rules import AnsibleLintRule
import re
import os


ok_filenames = [ 'authorized_keys', 'known_hosts', 'ssh_known_hosts','.pgpass' ]
ok_filepaths = [ '/etc/email-addresses','/etc/aliases' ]

class AvoidLineInFile(AnsibleLintRule):
    id = 'CHEM0001'
    shortdesc = 'Use template or blockinfile instead of lineinfile where possible'
    description = 'lineinfile is acceptable for files that are simple lists where ordering does' \
                  'not matter and the file format doesn''t support ' \
                  'comments. But even then consider using template instead.' 
    tags = ['usability']

    def matchtask(self, task, file):
        if task["action"]["__ansible_module__"] == 'lineinfile':
            try:
                dest = task["action"]["dest"]
            except KeyError:
                dest = task["action"]["path"]
            destfile = os.path.basename(dest)
            return not ((destfile in ok_filenames) or (dest in ok_filepaths))
