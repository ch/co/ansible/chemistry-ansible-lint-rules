# Copyright (c) 2013-2014 Will Thames <will@thames.id.au>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from ansiblelint.rules import AnsibleLintRule
import six


class EnsurePermissionsSetOnCreatedFiles(AnsibleLintRule):
    id = 'CHEM0007'
    shortdesc = 'Created files must have mode, owner and group'
    description = shortdesc
    tags = ['security']

    # list of modules based on those used by the lint rule which
    # checks for leading zero on octal modes, except ignoring:
    # 'unarchive' because that's rather difficult to enforce modes
    # 'synchronize' because 'owner' and 'group' have a rather different meaning
    _modules = ['assemble', 'copy', 'file', 'replace', 'template', 'get_url']

    # modules which we will ignore here if create: no
    _modules_with_create = ['lineinfile', 'ini_file', 'blockinfile']

    def matchtask(self, task, file):
        results = []

        if task["action"]["__ansible_module__"] in self._modules + self._modules_with_create:
            if task["action"]["__ansible_module__"] == 'file':
                state = task['action'].get('state', 'file')
                if state in ['absent', 'hard', 'link', 'touch']:
                    return False

            if task["action"]["__ansible_module__"] in self._modules_with_create:
                create = task['action'].get('create', False)
                if create == False:
                    return False

            mode = task['action'].get('mode', None)
            owner = task['action'].get('owner', None)
            group = task['action'].get('group', None)
            if not mode:
              #results.append('mode not set for {} module'.format(task['action']['__ansible_module__']))
              results.append('mode')
            if not owner:
              #results.append('owner not set for {} module'.format(task['action']['__ansible_module__']))
              results.append('owner')
            if not group:
              results.append('group')
              #results.append('group not set for {} module'.format(task['action']['__ansible_module__']))

            if results:
                return '{} module should set {}'.format(task['action']['__ansible_module__'], ', '.join(results))
