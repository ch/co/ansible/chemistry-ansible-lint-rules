from ansiblelint.rules import AnsibleLintRule

class IncludeVarsVaultNoLog(AnsibleLintRule):
    id = 'CHEM0005'
    shortdesc = 'When including vault variables nolog should be set'
    description = 'FIXME'
    tags= []

    def matchtask(self, task, file):
        if task['action']['__ansible_module__'] == 'include_vars':
            possible_places = [task.get('name',None)] + task['action']['__ansible_arguments__'] + [task['action'].get('file')]
            return self._find_logged_vaults(possible_places, task)

    def matchplay(self, file, play):
        if 'vars_files' in play:
            possible_places = play['vars_files']
            return self._find_logged_vaults(possible_places, play)
        return []

    def _find_logged_vaults(self, possibles, task):
        if 'no_log' in task:
            return False
        else:
            matches = [ (task.get('name',x),self.shortdesc) for x in possibles if x is not None and 'vault' in x ]
        return matches


