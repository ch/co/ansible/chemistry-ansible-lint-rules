# Custom ansible linter rules used in Chemistry

For ansible-lint version 5.1.x.
Note that our rule CHEM0007/"EnsurePermissionsSetOnCreatedFiles" is very similar to ansible-lint's new rule "risky-file-permissions".
Tests have been added for all other rules.

Specify the path to the custom rules with `-r` and add `-R` to use the default rules **in addition** to these custom rules:

```
ansible-lint -R -r /path/to/chemistry-ansible-lint-rules
```

The above will also check roles that aren't referenced anywhere.
To avoid that, specify the playbook. For example:

```
ansible-lint -R -r /path/to/chemistry-ansible-lint-rules site.yml
```
when dealing with `ansibleconf` (servers) and
```
ansible-lint -R -r /path/to/chemistry-ansible-lint-rules local.yml
```
when dealing with `ansible-xenial` (workstations).

