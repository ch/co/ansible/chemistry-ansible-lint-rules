from ansiblelint.rules import AnsibleLintRule

class TasksInRoles(AnsibleLintRule):
    id = 'CHEM0003'
    shortdesc = 'Tasks should go in roles not the main playbook'
    description = 'Tasks should normally go into roles rather than our top level ' + \
            'playbooks. This is to keep the playbooks simple and make the code reusable. '
    tags = ['reusability','readability']

    def matchplay(self, playbookfile, play):
        msg = "Found a task in a play, should be in a role"
        if playbookfile['type'] == 'playbook' and 'tasks' in play:
            tags = play.get('tags',[])
            if 'skip_ansible_lint' in tags:
                return []
            matches = []
            for t in play['tasks']:
                match = "Task '{}' found at line {}".format(t.get('name',''),t['__line__'])
                matches.append(self.create_matcherror(
                        message=match, filename=playbookfile, linenumber=t['__line__'], details=msg
                    ))
            return matches
        return []

