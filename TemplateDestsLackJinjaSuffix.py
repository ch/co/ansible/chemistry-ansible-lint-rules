from ansiblelint.rules import AnsibleLintRule

class TemplateDestsLackJinjaSuffix(AnsibleLintRule):
    id = 'CHEM0006'
    shortdesc = 'Template destinations should not end .j2'
    description = 'Destinations of templated files ending .j2 are usually ' \
                  'indicative of a copy/paste error from the src line'
    tags = ['safety']

    def matchtask(self, task, file):
        results = []
        if task['action']['__ansible_module__'] == 'template':
            if task['action']['dest'].endswith('.j2'):
                results.append(task['action']['dest'])

            if results:
                return "Template destinations {} end with .j2".format(' '.join(results))
        return False
