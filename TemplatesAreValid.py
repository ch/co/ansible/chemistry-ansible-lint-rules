import os
import re
import sys
from jinja2 import Environment
from ansiblelint.rules import AnsibleLintRule

class TemplatesAreValid(AnsibleLintRule):
    id = 'CHEM0008'
    shortdesc = 'Templates should be parseable'
    description = shortdesc
    tags = ['safety']

    DEFAULT_NEWLINE_SEQUENCE = "\n"

    def possible_paths(self,taskfile,template):
        # Find possible locations for the template file
        # This is only correct for Ansible 2.0 and takes no account of roles_path or
        # files defined with with_first_found or Jinja templates.
        # https://github.com/ansible/ansible/issues/14161 gives the search rule as
        # if in role:
        #  rolename/templates/<file>
        #  rolename/tasks/<file>
        #  rolename/<file>
        # always:
        #  play_dir/templates/<file>
        #  play_dir/<file>
        potential_paths = []
        in_role = False
        try:
            in_role = 'roles' in taskfile
        except IndexError:
            pass
        if in_role:
            role_dir = os.path.dirname(os.path.dirname(taskfile))
            for subdir in [ 'templates', 'tasks', '' ]:
                potential_paths.append(os.path.join(role_dir,subdir,template))
        dirname = os.path.dirname(taskfile)
        for subdir in [ 'templates','' ]:
            potential_paths.append(os.path.join(dirname,subdir,template))
        return [ x for x in potential_paths if os.path.isfile(x) ]

    def possible_templates(self,taskfile,task):
        # handle simple cases with loops, mostly lifted from UsingBareVariablesIsDeprecatedRule.py
        item_sub = re.compile(r'{{\s*item\s*}}')
        loop_type = next((key for key in task
                                      if (key.startswith("with_") or key.startswith("loop")) ), None)
        if loop_type:
            if loop_type in ["with_items", "with_random_choice", "loop"]:
                possibles = task[loop_type]
                return [ item_sub.sub(x,task['action']['src']) for x in possibles if not isinstance(x,dict)]
            elif loop_type in ["with_first_found"]:
                possibles = []
                for i in task[loop_type]:
                    if isinstance(i,dict):
                        possibles += [ os.path.join(x,y) for x in i.get('paths',[]) for y in i['files']]
                    else:
                        possibles += task[loop_type]
                return [ item_sub.sub(x,task['action']['src']) for x in possibles ]
            else:
                pass
        return [task['action']['src']]

    def matchtask(self, task, file):
        results = []
        if task['action']['__ansible_module__'] == 'template':
            options = {}

            if 'block_start_string' in task['action']:
                options['block_start_string'] = task['action']['block_start_string']
            if 'block_end_string' in task['action']:
                options['block_end_string'] = task['action']['block_end_string']

            if 'variable_start_string' in task['action']:
                options['variable_start_string'] = task['action']['variable_start_string']
            if 'variable_end_string' in task['action']:
                options['variable_end_string'] = task['action']['variable_end_string']

            env = Environment(**options)

            for filename in self.possible_templates(file,task):
                for template in self.possible_paths(file['path'],filename):
                    try:
                        with open(template,'r') as f:
                            env.parse(f.read())
                    except Exception as e:
                        results.append(template)
                        print("[Warning] {me} was unable to parse {template!r}; {exception}".format(me=os.path.basename(__file__), template=template, exception=e), file=sys.stderr)
            
            if results:
                return  "Invalid templates: {}".format(' '.join(results))
            else:
                return False

