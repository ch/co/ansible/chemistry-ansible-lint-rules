import os
import re
import sys
from ansiblelint.rules import AnsibleLintRule


class _TemplatesHaveTag(AnsibleLintRule):
    shortdesc = 'Templates should contain comment marking them as ansible managed'
    tags = ['safety']

    def __init__(self):
        self.old_marker_friendly = self.old_marker_str.replace(r'\s*', ' ').replace(r'\|', '|')
        self.old_marker = re.compile(self.old_marker_str)
        self.marker_friendly = self.marker_str.replace(r'\s*', ' ').replace(r'\|', '|')
        self.marker = re.compile(self.marker_str)

    def possible_paths(self,taskfile,template):
        # Find possible locations for the template file
        # This is only correct for Ansible 2.0 and takes no account of roles_path or
        # files defined with with_first_found or Jinja templates.
        # https://github.com/ansible/ansible/issues/14161 gives the search rule as
        # if in role:
        #  rolename/templates/<file>
        #  rolename/tasks/<file>
        #  rolename/<file>
        # always:
        #  play_dir/templates/<file>
        #  play_dir/<file>
        potential_paths = []
        in_role = False
        try:
            in_role = 'roles' in taskfile
        except IndexError:
            pass
        if in_role:
            role_dir = os.path.dirname(os.path.dirname(taskfile))
            for subdir in [ 'templates', 'tasks', '' ]:
                potential_paths.append(os.path.join(role_dir,subdir,template))
        dirname = os.path.dirname(taskfile)
        for subdir in [ 'templates','' ]:
            potential_paths.append(os.path.join(dirname,subdir,template))
        return [ x for x in potential_paths if os.path.isfile(x) ]

    def possible_templates(self,taskfile,task):
        # handle simple cases with loops, mostly lifted from UsingBareVariablesIsDeprecatedRule.py
        item_sub = re.compile(r'{{\s*item\s*}}')
        loop_type = next((key for key in task
                                      if (key.startswith("with_") or key.startswith("loop")) ), None)
        if loop_type:
            if loop_type in ["with_items", "with_random_choice", "loop"]:
                possibles = task[loop_type]
                return [ item_sub.sub(x,task['action']['src']) for x in possibles if not isinstance(x,dict)]
            elif loop_type in ["with_first_found"]:
                possibles = []
                for i in task[loop_type]:
                    if isinstance(i,dict):
                        possibles += [ os.path.join(x,y) for x in i.get('paths',[]) for y in i['files']]
                    else:
                        possibles += task[loop_type]
                return [ item_sub.sub(x,task['action']['src']) for x in possibles ]
            else:
                pass
        return [task['action']['src']]

    def matchtask(self, task, file):
        results = []
        if task['action']['__ansible_module__'] == 'template':
            for filename in self.possible_templates(file,task):
                for template in self.possible_paths(file['path'],filename):
                    try:
                        with open(template,'r') as f:
                            template_contents = f.read()
                            if not self.old_marker.search(template_contents) and not self.marker.search(template_contents):
                                results.append(template)
                    except Exception as e:
                        #raise RuntimeError("Unable to parse {template!r}".format(template=template)) from e
                        print("[Warning] {me} was unable to parse {template!r}; {exception}".format(me=os.path.basename(__file__), template=template, exception=e), file=sys.stderr)
            if results:
                return  self.marker_friendly + " not found in {}".format(' '.join(results))


class TemplatesHaveTag(_TemplatesHaveTag):
    id = 'CHEM0004'
    marker_str = r'Ansible managed:\s*{{\s*template_fullpath\s*\|\s*chem_managed\s*}}'
    # We need two marker_str patterns as we still permit two variants in the workstation
    # repo. But for servers we only support one pattern now.
    old_marker_str = marker_str
    description = 'Templated files should be marked as such to avoid people accidentally ' + \
            'overwriting them on live systems. Use Ansible managed: {{ template_fullpath | chem_managed }} ' + \
            'inside a suitable comment unless the file cannot contain comments.'


class TemplatesHaveTagWorkstations(_TemplatesHaveTag):
    id = 'CHEM0004w'
    old_marker_str = r'{{\s*ansible_managed\s*}}'
    marker_str = r'Ansible managed:\s*{{\s*template_fullpath\s*\|\s*chem_managed\s*}}'
    description = 'Templated files should be marked as such to avoid people accidentally ' + \
            'overwriting them on live systems. Use {{ ansible_managed }} or :' + \
            'Ansible managed: {{ template_fullpath | chem_managed }}' + \
            'inside a suitable comment unless the file cannot contain comments.'
